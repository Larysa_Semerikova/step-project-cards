export class Filter {
  
  showDoctor() {
    const doctorName = document.querySelector(".select-doctors");
    const urgencyName = document.querySelector(".select-urgency");
    doctorName.addEventListener("change", () => {
      let urgency = urgencyName.value;
      let doctors = doctorName.value;
     const allCards = document.querySelectorAll(".card-body");
     Array.from(allCards).forEach((pacient) => {
      if (doctors === "all-doctors" && urgency === pacient.dataset.urgency){
        pacient.style.display = "block";
      }  else if (doctors === "all-doctors" && urgency === "all-urgency") {
        pacient.style.display = "block";
      }
        else if (doctors == pacient.dataset.doctor && urgency === "all-urgency") {
        pacient.style.display = "block";
      } else if (urgency === pacient.dataset.urgency && doctors == pacient.dataset.doctor ) {
        pacient.style.display = "block";
      } else{
        pacient.style.display = "none";
      } 
    })
    });
  }

  showUrgency() {
    const doctorName = document.querySelector(".select-doctors");
    const urgencyName = document.querySelector(".select-urgency");
    urgencyName.addEventListener("change", () => {
      let urgency = urgencyName.value;
      let doctors = doctorName.value;
     const allCards = document.querySelectorAll(".card-body");
     Array.from(allCards).forEach((pacient) => {      
      if (urgency === "all-urgency"  && doctors == pacient.dataset.doctor){
        pacient.style.display = "block";
      } else if (doctors === "all-doctors" && urgency === "all-urgency") {
        pacient.style.display = "block";
      } else if (urgency === pacient.dataset.urgency  && doctors === "all-doctors"){
        pacient.style.display = "block";
      }
        else if (urgency === pacient.dataset.urgency && doctors == pacient.dataset.doctor ) {
        pacient.style.display = "block";
      }  else{
        pacient.style.display = "none";
      } 
    })
    });
  }

  showPacient() {
    this.namePacient = document.querySelector(".form-control");
    this.namePacient.addEventListener("input", () => {
      let pacientNameStr = this.namePacient.value;
      const allNamesPacient = document.querySelectorAll(".card-body");
      
      Array.from(allNamesPacient).forEach((pacient) => {
        if (
          pacient.innerText
            .toLowerCase()
            .includes(pacientNameStr.toLowerCase(), 5)
        ) {
          pacient.style.display = "block";
        } else {
          pacient.style.display = "none";
        }
      });
    });
  }

  
  render() {
    this.showDoctor();
    this.showPacient();
    this.showUrgency();
    
  }
}
const filter = new Filter();
filter.render();
