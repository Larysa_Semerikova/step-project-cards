import { Modal} from "./modal.js";



export class EditCardFormDentist extends Modal{
    constructor(idModal="editCardFormDentist", label, title="Редагувати візит"){
      super(idModal, label, title);
      this.body =`
      <form class="g-3 needs-validation editForm" novalidate>
      <div class="mb-3 doctors-select">
          <select name="doctor" id="doctorSelect" class="form-select" aria-label="Default select example">
               <option value="dentist" class="dentistValue">Стоматолог</option>
          </select>
    </div>
      <div id="allDoctors">
          <div class="input-group">
              <span class="input-group-text">ПІБ</span>
              <input type="text" aria-label="First name" class="form-control" name="fullName" id="fullName">
      </div>
      <div class="mb-3 urgency-select">
          <select name="urgency" class="form-select" aria-label="Default select example" id="urgencySelect">
            <option selected>Терміновість</option>
            <option value="Звичайна">Звичайна</option>
            <option value="Пріоритетна">Пріоритетна</option>
            <option value="Невідкладна">Невідкладна</option>
          </select>
      </div>
      <div class="input-group">
          <span class="input-group-text">Мета візиту</span>
          <input type="text" aria-label="First name" class="form-control" name="purpose" id="purpose">
      </div>
      <div class="mb-3">
      <span class="input-group-text">Короткий опис візиту</span>
          <textarea class="form-control" id="desc" rows="3" name="desc"></textarea>
      </div>
      <div class="mb-3" id="additionalFields">
          <div class="input-group">
          <span class="input-group-text">Дата останнього візиту</span>
          <input type="date" name="lastVisitDate" id="lastVisitDate"/>
      </div>
      </div>
      <div class="mb-3 d-flex justify-content-end">
          <button type="submit" id="edit-btn" class="btn btn-primary ms-3">Підтвердити</button>
      </div>
    </div>
      </form>`
  }
}

export class EditCardFormCardiologist extends Modal{
  constructor(idModal="editCardFormCardiologist", label, title="Редагувати візит"){
    super(idModal, label, title);
    this.body =`
    <form class="g-3 needs-validation editForm" novalidate>
    <div class="mb-3 doctors-select">
        <select name="doctor" id="doctorSelect" class="form-select" aria-label="Default select example">
        <option value="cardiologist">Кардіолог</option>
        </select>
  </div>
    <div id="allDoctors">
        <div class="input-group">
            <span class="input-group-text">ПІБ</span>
            <input type="text" aria-label="First name" class="form-control" name="fullName" id="fullName">
    </div>
    <div class="mb-3 urgency-select">
        <select name="urgency" class="form-select" aria-label="Default select example" id="urgencySelect">
          <option selected>Терміновість</option>
          <option value="Звичайна">Звичайна</option>
          <option value="Пріоритетна">Пріоритетна</option>
          <option value="Невідкладна">Невідкладна</option>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-text">Мета візиту</span>
        <input type="text" aria-label="First name" class="form-control" name="purpose" id="purpose">
    </div>
    <div class="mb-3">
    <span class="input-group-text">Короткий опис візиту</span>
        <textarea class="form-control" id="desc" rows="3" name="desc"></textarea>
    </div>
    <div class="mb-3" id="additionalFields">
        <div class="input-group">
        <span class="input-group-text">Звичайний тиск</span>
        <input type="text" aria-label="First name" class="form-control" name="pressure1" id="pressure1" placeholder="систолічний">
        <input type="text" aria-label="Last name" class="form-control" name="pressure2" id="pressure2" placeholder="діастолічний">
    </div>
    <div class="input-group">
        <span class="input-group-text">Індекс маси тіла</span>
        <input type="text" aria-label="First name" class="form-control" name="bmi" id="bmi">
    </div>
    <div class="input-group">
        <span class="input-group-text">Вік</span>
        <input type="text" aria-label="First name" class="form-control" name="age" id="age">
    </div>
        <span class="input-group-text">Перенесені серцево-судинні захворювання</span>
        <textarea class="form-control" name="deseases" rows="3" id="deseases"></textarea>
    </div>
    <div class="mb-3 d-flex justify-content-end">
        <button type="submit" id="edit-btn" class="btn btn-primary ms-3">Підтвердити</button>
    </div>
  </div>
    </form>`
}
}
export class EditCardFormTherapist extends Modal{
  constructor(idModal="editCardFormTherapist", label, title="Редагувати візит"){
    super(idModal, label, title);
    this.body =`
    <form class="g-3 needs-validation editForm" novalidate>
    <div class="mb-3 doctors-select">
        <select name="doctor" id="doctorSelect" class="form-select" aria-label="Default select example">
        <option value="therapist">Терапевт</option>
        </select>
  </div>
    <div id="allDoctors">
        <div class="input-group">
            <span class="input-group-text">ПІБ</span>
            <input type="text" aria-label="First name" class="form-control" name="fullName" id="fullName">
    </div>
    <div class="mb-3 urgency-select">
        <select name="urgency" class="form-select" aria-label="Default select example" id="urgencySelect">
          <option selected>Терміновість</option>
          <option value="Звичайна">Звичайна</option>
          <option value="Пріоритетна">Пріоритетна</option>
          <option value="Невідкладна">Невідкладна</option>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-text">Мета візиту</span>
        <input type="text" aria-label="First name" class="form-control" name="purpose" id="purpose">
    </div>
    <div class="mb-3">
    <span class="input-group-text">Короткий опис візиту</span>
        <textarea class="form-control" rows="3" name="desc" id="desc"></textarea>
    </div>
    <div class="mb-3" id="additionalFields">
    <div class="input-group">
        <span class="input-group-text">Вік</span>
        <input type="text" aria-label="First name" class="form-control" name="age" id="age">
    </div>
    </div>
    <div class="mb-3 d-flex justify-content-end">
        <button type="submit" id="edit-btn" class="btn btn-primary ms-3">Підтвердити</button>
    </div>
  </div>
    </form>`
}
}