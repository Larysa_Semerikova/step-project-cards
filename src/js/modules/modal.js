export class Modal {
  constructor(idModal, label, title, body) {
    this.idModal = idModal;
    this.label = label;
    this.title = title;
    this.body = body;
  }

  render() {
    const modalParent = document.querySelector(".root");
    document.body.insertAdjacentHTML(
      "beforeend",
      `<div class="modal-backdrop fade show"></div>`
    );
    modalParent.insertAdjacentHTML(
      "afterend",
      `
        <div class="modal" tabindex="-1" id="${this.idModal}" aria-labelledby="${this.label}" aria-hidden="true" style="display:block">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="${this.label}">${this.title}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <p class="invalid-message text-center text-danger mt-2 mb-0"></p>
                    <div class="modal-body">
                        ${this.body}
                    </div>
                </div>
            </div>
        </div>
        `
    );
  }
}

export class ModalLogin extends Modal {
  constructor(
    idModal = "authorization-form-modal",
    label = "authorizationModalLabel",
    title = "Авторизація",
    email = "",
    password = ""
  ) {
    super(idModal, label, title);
    this.email = email;
    this.password = password;
    this.body = `
    <form id="login-form">
        <div class="mb-3 form-floating">
            <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="email" required value="${this.email}">
            <label for="inputEmail" class="form-label">Введіть електронну пошту</label>
        </div>
        <div class="mb-3 form-floating">
            <input type="password" class="form-control" id="inputPassword" value="${this.password}" placeholder="password" required autocomplete="on">
            <label for="inputPassword" class="form-label">Введіть пароль</label>
        </div>
        <div class="mb-3 d-flex justify-content-end">
            <button type="submit" id="login-btn" class="btn btn-send ms-3">Підтвердити</button>
        </div>
    </form>
    `;
  }

  emptyInputMessage() {
    const invalidMessage = document.querySelector(".invalid-message");
    invalidMessage.innerHTML = "Ви ввели не всі дані!";
  }
  removeInvalidMessage() {
    const invalidMessage = document.querySelector(".invalid-message");
    invalidMessage.innerHTML = "";
  }
}

export class ModalCreateCard extends Modal {
  constructor(
    idModal = "create-form-modal",
    label = "createModalLabel",
    title = "Новий запис"
  ) {
    super(idModal, label, title);
    this.body = `
    <form id="newVisitForm" class="g-3 needs-validation" novalidate>
        <div class="mb-3 doctors-select">
            <select name="doctor" id="doctorSelect" class="form-select" aria-label="Default select example">
                 <option selected value="all">Лікар</option>
                 <option value="cardiologist">Кардіолог</option>
                 <option value="dentist" class="dentistValue">Стоматолог</option>
                 <option value="therapist">Терапевт</option>
            </select>
      </div>

        <div class="hidden" id="allDoctors">
            <div class="input-group">
                <span class="input-group-text">ПІБ</span>
                <input type="text" aria-label="First name" class="form-control" name="fullName">
        </div>

        <div class="mb-3 urgency-select">
            <select name="urgency" class="form-select" aria-label="Default select example" id="urgencySelect">
              <option selected>Терміновість</option>
              <option value="Звичайна">Звичайна</option>
              <option value="Пріоритетна">Пріоритетна</option>
              <option value="Невідкладна">Невідкладна</option>
            </select>
        </div>

        <div class="input-group">
            <span class="input-group-text">Мета візиту</span>
            <input type="text" aria-label="First name" class="form-control" name="purpose">
        </div>

        <div class="mb-3">
        <span class="input-group-text">Короткий опис візиту</span>
            <textarea class="form-control" id="name" rows="3" name="desc"></textarea>
        </div>

        <div class="mb-3" id="additionalFields">
            <div class="mb-3" id="cardio"></div>
            <div class="mb-3" id="dentist"></div>
            <div class="mb-3" id="therapist"></div>
        </div>

        
        <div class="mb-3 d-flex justify-content-end">
            <button type="submit" id="confirm-btn" class="btn btn-primary ms-3">Підтвердити</button>
        </div>

      </div>
        </form>
        `;
  }
  render() {
    super.render();

    const form = document.querySelector("#newVisitForm");
    const allDoctorsBlock = form.querySelector("#allDoctors");
    const cardiologistBlock = form.querySelector("#cardio");
    const dentistBlock = form.querySelector("#dentist");
    const therapistBlock = form.querySelector("#therapist");

    form.addEventListener("click", (e) => {
        if (e.target.value === "all"){
            allDoctorsBlock.style.display = "none";
        } else if (e.target.value === "cardiologist") {
          allDoctorsBlock.style.display = "block";
          dentistBlock.style.display = "none";
          therapistBlock.style.display = "none";
          cardiologistBlock.style.display = "block"
          cardiologistBlock.innerHTML = `
                    <div class="input-group">
                    <span class="input-group-text">Звичайний тиск</span>
                    <input type="text" aria-label="First name" class="form-control" name="pressure1" placeholder="систолічний">
                    <input type="text" aria-label="Last name" class="form-control"name="pressure2" placeholder="діастолічний">
                    </div>
                    <div class="input-group">
                    <span class="input-group-text">Індекс маси тіла</span>
                    <input type="text" aria-label="First name" class="form-control" name="bmi">
                    </div>
                    <div class="input-group">
                    <span class="input-group-text">Вік</span>
                    <input type="text" aria-label="First name" class="form-control" name="age">
                    </div>
                    <span class="input-group-text">Перенесені серцево-судинні захворювання</span>
                    <textarea class="form-control" name="deseases" rows="3"></textarea>
                    `;
        } else if (e.target.value === "dentist") {
          allDoctorsBlock.style.display = "block";
          cardiologistBlock.style.display = "none";
          therapistBlock.style.display = "none";
          dentistBlock.style.display = "block"
          dentistBlock.innerHTML = `
                    <div class="input-group">
                    <span class="input-group-text">Дата останнього візиту</span>
                    <input type="date" name="lastVisitDate"/>
                    </div>
                    `;
        } else if (e.target.value === "therapist") {
          allDoctorsBlock.style.display = "block";
          cardiologistBlock.style.display = "none";
          dentistBlock.style.display = "none"
          therapistBlock.style.display = "block";
          therapistBlock.innerHTML = `
                    <div class="input-group">
                    <span class="input-group-text">Вік</span>
                    <input type="text" aria-label="First name" class="form-control" name="age">
                    </div>
                    `;
        }
      
    });
  }
}

