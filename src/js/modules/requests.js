const tokenAPI = 'https://ajax.test-danit.com/api/v2/cards/login';
const API = 'https://ajax.test-danit.com/api/v2/cards';
const noCards = document.querySelector('.no-cards');
let token = "e18de9a2-a852-495b-afd1-e9370a5f5b41"
let allVisits=[]
import {VisitCardiologist,VisitTherapist,VisitDentist } from "./visit.js";
import { cardBlock } from "./visit.js";
import { renderNewCard } from "../app.js";

export async function getToken(email, password) {
    const response = await fetch(tokenAPI, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,
            password: password,
        }),
    });

    if (response.ok) {
        return response.text();
    } else {
        alert('Користувача з такими даними не зареєстровано!');
    }
}


const sendRequest = async (API, lastPartAPI = '', method = 'GET', options) => {
    return await fetch(`${API}${lastPartAPI}`, {
        method,
        ...options
    })
    .then(response => {
        if (response.ok) {
            if (method !== "DELETE") {
                const result = response.json();
                return result;
            } else {
                return response;
            }
        } else {
            return new Error("Error");
        }
    });
};


export async function getAllCards(API, token) {
    fetch(API, {
        method: "GET",
        headers: {
            Authorization: `Bearer ${token}`
        }
    
    })
    .then((response) => response.json())
      .then((response) => {

        console.log(response);
         const checkAvailabilityCardsOnBord = response.length;
         if (checkAvailabilityCardsOnBord === 0) {
            noCards.classList.remove("d-none");
         }
        console.log(checkAvailabilityCardsOnBord); 
        response.forEach((elem) => {
          if (elem.doctor == "cardiologist") {
            let cardiologist = new VisitCardiologist();
            cardiologist.render(elem);
          }else if (elem.doctor == "therapist") {
            let therapist = new VisitTherapist();
            therapist.render(elem);
          }else if (elem.doctor == "dentist") {
            let therapist = new VisitDentist();
            therapist.render(elem);
          }
        });
      });
  }

export async function sendCard(API, token) {
        const form = document.querySelector("#newVisitForm")
        form.addEventListener("submit", (e) => {
            const formData = new FormData(form);
            const res = Object.fromEntries(formData);
            const payload = JSON.stringify(res);
        
            fetch(API, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: payload,
            })
            .then(res=> res.json())
            .then(res => {{allVisits.push(res);
                localStorage['allVisits'] = JSON.stringify(allVisits);}})
            })
}


export async function delCard(cardId) {{
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });       
    }
}

export async function getOneCard(API, token, cardId) {
    sendRequest(API, `/${cardId}`, ...[,], {
    headers: {
        'Authorization': `Bearer ${token}`
      }
    });
}

export async function editCard(cardId) {
    const formTherapist = document.querySelector(".editForm")
            console.log(formTherapist)
            formTherapist.addEventListener("submit", (e) => {
                const formData = new FormData(formTherapist);
                const res = Object.fromEntries(formData);
                const payload = JSON.stringify(res);
                fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: payload,
                })
                .then(res=> res.json())
                .then(res => {{allVisits.push(res);
                    localStorage['allVisits'] = JSON.stringify(allVisits);}})
                })
}







