import {  Modal, ModalLogin } from './modal.js';
import { getToken, getAllCards } from './requests.js';
let visitsCollection =[]
const API = 'https://ajax.test-danit.com/api/v2/cards'

export function LOGIN() {
    const btnEnter = document.querySelector('#entry-btn');
    const btnLogout = document.querySelector('#exit-btn');
    const noAuthorization = document.querySelector('.no-authorization')

    function showSomeDetails() {
        const createVisitBtn = document.querySelector('#create-btn');
        const filters = document.querySelectorAll('.wrapper-filter');
        const cards = document.querySelector('.card');
        btnEnter.classList.add('d-none');
        noAuthorization.classList.add('d-none')
        btnLogout.classList.remove('d-none');
        createVisitBtn.classList.remove('d-none');            
        Array.from(filters).forEach((elem)=>{
           elem.classList.remove('d-none');
        })
        cards.classList.remove('d-none');
    }

    if (!localStorage.getItem('token')) {

        btnEnter.addEventListener('click', () => {
            if (!document.querySelector('#authorization-form-modal')) {
                const form = new ModalLogin();
                form.render();
                
    
                const modalForm = document.querySelector('#authorization-form-modal');
                const emailForm = modalForm.querySelector('input[type="email"]');
                const passwordForm = modalForm.querySelector('input[type="password"]');
                const btnForm = modalForm.querySelector('.btn-send');
    
                btnForm.addEventListener('click', (e) => {
                    e.preventDefault();
                    if (emailForm.value && passwordForm.value) {
                        submitForm(modalForm, emailForm.value, passwordForm.value);
                        form.removeInvalidMessage();
                    } else {
                        form.emptyInputMessage();
                    }
                });
    
                modalForm.addEventListener('click', (e) => {
                    
                    if (e.target.classList.contains('modal') || e.target.classList.contains('btn-close')) {
                        modalForm.remove()

                        const modalBackDrop = document.querySelector('.modal-backdrop');
                        modalBackDrop.remove();
                        emailForm.value = '';
                        passwordForm.value = '';
                        form.removeInvalidMessage();
                    }
                });


            } else {
                const modalForm = document.querySelector('#authorization-form-modal');
                modalForm.style.display = 'block';
                document.body.insertAdjacentHTML('beforeend',`<div class="modal-backdrop fade show"></div>`);
            }
        });

    
        async function submitForm(form, email, password) {
            const token = await getToken(email, password);
            localStorage.token = token;
    
            if (token) {
                showSomeDetails();

                form.style.display = 'none';
    
                const modalBackDrop = document.querySelector('.modal-backdrop');
                modalBackDrop.remove();

                fetch(API, {
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                  })
                  .then(res=> res.json())
                  .then(result => {localStorage.setItem('allVisits', JSON.stringify(result))
                  console.log(result)
                })

    
            }
        }

    } else {
        showSomeDetails();
    }
    
    btnLogout.addEventListener('click', () => {
        localStorage.removeItem('token')
        window.location.reload()
    })

}


