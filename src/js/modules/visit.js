export const cardBlock = document.querySelector(".card");
let token = "e18de9a2-a852-495b-afd1-e9370a5f5b41";
import { delCard, editCard} from "./requests.js";
import {
  EditCardFormDentist,
  EditCardFormCardiologist,
  EditCardFormTherapist,
} from "./editCard.js";

class Component {
  constructor(parentElement, position) {
    this.parentElement = parentElement;
    this.position = position;
  }
  createElement(newElem) {
    if (typeof newElem === "string") {
      this.parentElement.insertAdjacentHTML(this.position, newElem);
    } else if (typeof newElem === "object") {
      this.tagName = newElem.tagName;
      this.classNames = newElem.classNames;
      this.parentElement = newElem.parentElement;
      this.content = newElem.content;
      this.values = newElem.values;
      this.key = newElem.key;
      this.position = newElem.position;
      this.createElementbyObj();
    }
    return this.element;
  }

  createElementbyObj() {
    this.element = document.createElement(this.tagName);
    this.parentElement.append(this.element);

    this.position === "prepend"
      ? this.parentElement.prepend(this.element)
      : this.parentElement.append(this.element);

    if (this.values && this.key) {
      this.element.setAttribute(this.key, this.values);
    }
    if (this.classNames) {
      Array.isArray(this.classNames)
        ? this.element.classList.add(...this.classNames)
        : this.element.classList.add(this.classNames);
    }

    if (this.content) {
      this.element.innerHTML = this.content;
    }

    return this.element;
  }
}

class Visit extends Component {
  getStatus(response) {
    this.status = response.status;
  }
  getName(response) {
    this.fullName = response.fullName;
  }
  getPurpose(response) {
    this.purpose = response.purpose;
  }
  getDescription(response) {
    this.desc = response.desc;
  }
  getUrgency(response) {
    this.urgency = response.urgency;
  }
  renderDefaultVisit(response) {
    this.getStatus(response);
    this.getName(response);
    this.getPurpose(response);
    this.getDescription(response);
    this.getUrgency(response);
  }
}

export class VisitCardiologist extends Visit {
  getPressure(response) {
    this.pressure1 = response.pressure1;
    this.pressure2 = response.pressure2;
  }
  getID(response) {
    this.id = response.id;
  }
  getDoctor(response) {
    this.doctor = response.doctor;
  }
  getDeseases(response) {
    this.deseases = response.deseases;
  }
  getAge(response) {
    this.age = response.age;
  }
  getBmi(response) {
    this.bmi = response.bmi;
  }
  showCard() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(".card");
    this
      .createElement(`<div data-item=${this.id} data-doctor=${this.doctor} data-urgency =${this.urgency} class=' visit-cardiologist-card card-body visit-card-element'>
      <div class='delete-card d-flex justify-content-end delete-card-${this.id}'> X </div>
      <div class='name'>Пацієнт: ${this.fullName}</div>
      <div class='visit-doctor'>Лікар: Кардіолог</div>  
      </div>`);
  }
  createBtnShowMore() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(
      `.visit-cardiologist-card[data-item="${this.id}"]`
    );
    this.createElement(`
      <button data-item=${this.id} type='submit' class='btn show-more'>Показати більше</button>
      `);
    this.btnShowMore = document.querySelector(
      `.show-more[data-item="${this.id}"]`
    );
    this.btnShowMore.addEventListener("click", () => {
      this.createElement(`
        
        <div class='description'>Опис візиту: ${this.desc}</div>
        <div class='purpose'>Мета візиту: ${this.purpose}</div>
        <div class='diseases'>Перенесені захворювання серцево-судинної системи: ${this.deseases}</div>
        <div class='purpose'>Звичайний тиск: ${this.pressure1} / ${this.pressure2}</div>
        <div class='purpose'>Індекс маси тіла: ${this.bmi}</div>
        <div class='age'>Вік: ${this.age}</div>
        <div class='visit-number'>Номер візиту: ${this.id}</div> 
        <div class = 'urgency'>Терміновість: ${this.urgency}  </div> 
        <button type='submit' class = 'btn edit edit-card-${this.id}'>Редагувати</button>
        `);
      this.btnShowMore.remove();
      this.editCard();
    });
  }
  deleteCard() {
    this.delete = document.querySelector(`.delete-card-${this.id}`);
    this.delete.addEventListener("click", () => {
      delCard(this.id);
      const visitCardElement = document.querySelector(".visit-card-element");
      visitCardElement.remove();
    });
  }
  editCard() {
    this.edit = document.querySelector(`.edit-card-${this.id}`);
    this.edit.addEventListener("click", () => {
      const editCardForm = new EditCardFormCardiologist();
      editCardForm.render(this.id);
      let name = (document.getElementById(
        "fullName"
      ).value = `${this.fullName}`);
      let urgency = (document.getElementById(
        "urgencySelect"
      ).value = `${this.urgency}`);
      let purpose = (document.getElementById(
        "purpose"
      ).value = `${this.purpose}`);
      let desc = (document.getElementById("desc").value = `${this.desc}`);
      let age = (document.getElementById("age").value = `${this.age}`);
      let pressure1 = (document.getElementById(
        "pressure1"
      ).value = `${this.pressure1}`);
      let pressure2 = (document.getElementById(
        "pressure2"
      ).value = `${this.pressure2}`);
      let bmi = (document.getElementById("bmi").value = `${this.bmi}`);
      let deseases = (document.getElementById(
        "deseases"
      ).value = `${this.deseases}`);
      editCard(this.id);
    });
  }
  render(response) {
    this.getID(response);
    this.getDoctor(response);
    this.renderDefaultVisit(response);
    this.getPressure(response);
    this.getDeseases(response);
    this.getAge(response);
    this.showCard();
    this.createBtnShowMore();
    this.deleteCard();
    this.getBmi(response);
  }
}

export class VisitTherapist extends Visit {
  getAge(response) {
    this.age = response.age;
  }
  getID(response) {
    this.id = response.id;
  }
  getDoctor(response) {
    this.doctor = response.doctor;
  }
  showCard() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(".card");
    this
      .createElement(`<div data-item=${this.id} data-doctor=${this.doctor} data-urgency =${this.urgency}  class=' visit-therapist-card card-body visit-card-element'>
    <div class='delete-card d-flex justify-content-end delete-card-${this.id}'> X </div>
    <div class='name'>Пацієнт: ${this.fullName}</div>
    <div class='visit-doctor'>Лікар: Терапевт</div>  
    </div>`);
  }
  createBtnShowMore() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(
      `.visit-therapist-card[data-item="${this.id}"]`
    );
    this.createElement(`
      <button data-item=${this.id} type='submit' class='btn show-more'>Показати більше</button>
      `);
    this.btnShowMore = document.querySelector(
      `.show-more[data-item="${this.id}"]`
    );
    this.btnShowMore.addEventListener("click", () => {
      this.createElement(`
        <div class='purpose'>Мета візиту: ${this.purpose}</div>
        <div class='description'>Опис визиту: ${this.desc}</div>
        <div class='lastVisit'>Вік: ${this.age}</div>
        <div class='visit-number'>Номер визиту: ${this.id}</div> 
        <div class = 'urgency'>Терміновість: ${this.urgency}  </div>  
        <button type='submit' class = 'btn edit edit-card-${this.id}'>Редагувати</button>
        `);
      this.btnShowMore.remove();
      this.editCard();
    });
  }
  deleteCard() {
    this.delete = document.querySelector(`.delete-card-${this.id}`);
    this.delete.addEventListener("click", () => {
      delCard(this.id);
      const visitCardElement = document.querySelector(".visit-card-element");
      visitCardElement.remove();
    });
  }
  editCard() {
    this.edit = document.querySelector(`.edit-card-${this.id}`);
    this.edit.addEventListener("click", () => {
      const editCardForm = new EditCardFormTherapist();
      editCardForm.render(this.id);
      let name = (document.getElementById(
        "fullName"
      ).value = `${this.fullName}`);
      let urgency = (document.getElementById(
        "urgencySelect"
      ).value = `${this.urgency}`);
      let purpose = (document.getElementById(
        "purpose"
      ).value = `${this.purpose}`);
      let desc = (document.getElementById("desc").value = `${this.desc}`);
      let age = (document.getElementById("age").value = `${this.age}`);
      editCard(this.id);
    });
  }
  render(response) {
    this.getID(response);
    this.getDoctor(response);
    this.renderDefaultVisit(response);
    this.getAge(response);
    this.showCard();
    this.createBtnShowMore();
    this.deleteCard();
  }
}

export class VisitDentist extends Visit {
  getLastVisit(response) {
    this.lastVisitDate = response.lastVisitDate;
  }
  getID(response) {
    this.id = response.id;
  }
  getDoctor(response) {
    this.doctor = response.doctor;
  }
  showCard() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(".card");
    this
      .createElement(`<div data-item=${this.id} data-doctor=${this.doctor} data-urgency =${this.urgency}  class='visit-dentist-card card-body visit-card-element'>
      <div class='delete-card d-flex justify-content-end delete-card-${this.id}'> X </div>
      <div class='name'>Пацієнт: ${this.fullName}</div>
      <div class='visit-doctor'>Лікар: Стоматолог</div> 
      </div>`);
  }
  createBtnShowMore() {
    this.position = "beforeend";
    this.parentElement = document.querySelector(
      `.visit-dentist-card[data-item="${this.id}"]`
    );
    this.createElement(`
      <button data-item=${this.id} type='submit' class=' btn show-more'>Показати більше</button>
      `);
    this.btnShowMore = document.querySelector(
      `.show-more[data-item="${this.id}"]`
    );
    this.btnShowMore.addEventListener("click", () => {
      this.createElement(`
        <div class='purpose'>Мета візиту: ${this.purpose}</div>
        <div class='description'>Опис візиту: ${this.desc}</div>
        <div class='lastVisit'>Дата останнього візиту: ${this.lastVisitDate}</div>
        <div class='visit-number'>Номер визиту: ${this.id}</div> 
        <div class = 'urgency'>Терміновість: ${this.urgency}  </div>  
        <button type='submit' class = 'btn edit edit-card-${this.id}'>Редагувати</button>
        `);
      this.btnShowMore.remove();
      this.editCard();
    });
  }
  deleteCard() {
    this.delete = document.querySelector(`.delete-card-${this.id}`);
    this.delete.addEventListener("click", () => {
      delCard(this.id);
      const visitCardElement = document.querySelector(".visit-card-element");
      visitCardElement.remove();
    });
  }
  editCard() {
    this.edit = document.querySelector(`.edit-card-${this.id}`);
    this.edit.addEventListener("click", () => {
      const editCardForm = new EditCardFormDentist();
      editCardForm.render(this.id);
      let name = (document.getElementById(
        "fullName"
      ).value = `${this.fullName}`);
      let urgency = (document.getElementById(
        "urgencySelect"
      ).value = `${this.urgency}`);
      let purpose = (document.getElementById(
        "purpose"
      ).value = `${this.purpose}`);
      let desc = (document.getElementById("desc").value = `${this.desc}`);
      let lastVisitDate = (document.getElementById(
        "lastVisitDate"
      ).value = `${this.lastVisitDate}`);
      editCard(this.id);
    });
  }
  render(response) {
    this.getID(response);
    this.getDoctor(response);
    this.renderDefaultVisit(response);
    this.getLastVisit(response);
    this.showCard();
    this.createBtnShowMore();
    this.deleteCard();
  }
}
